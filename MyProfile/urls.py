from django.urls import path
from .views import home, misc, createtask, tasklist

urlpatterns = [
    path('', home, name="Home"),
    path('misc/', misc, name="Misc"),
    path('createtask/', createtask, name="Create Task"),
    path('tasklist/', tasklist, name="Task List"),
]