from django import forms
from . import models

class TaskForm(forms.ModelForm):
    kategori=forms.CharField(widget=forms.TextInput(attrs={
        "style":"width:400px; margin:20px",
        "class":"form-control",
        "required":True,
        "placeholder":"Fill category here",
    }), label="Category")
    time=forms.DateField(widget=forms.DateInput(attrs={
        "style":"width:400px; margin:20px",
        "class":"form-control",
        "required":True,
        "type":"date",
    }), label="Due date")
    hour=forms.TimeField(widget=forms.TimeInput(attrs={
        "style":"width:400px; margin:20px",
        "class":"form-control",
        "required":True,
        "type":"time",
    }), label="Due hour")
    task=forms.CharField(widget=forms.TextInput(attrs={
        "style":"width:400px; margin:20px",
        "class":"form-control",
        "required":True,
        "placeholder":"Fill your task here",
    }), label="Task")
    tempat=forms.CharField(widget=forms.TextInput(attrs={
        "style":"width:400px; margin:20px",
        "class":"form-control",
        "required":True,
        "placeholder":"Fill place here",
    }), label="Place")
    class Meta:
        model=models.Task
        fields=['kategori','time','hour','task','tempat']