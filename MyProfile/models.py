from django.db import models

# Create your models here.
class Task(models.Model):
    kategori=models.CharField(max_length=100)
    time=models.DateField(blank=False)
    day=models.CharField(max_length=9)
    hour=models.TimeField(blank=False)
    task=models.CharField(max_length=100)
    tempat=models.CharField(max_length=100)
    date=models.DateTimeField(auto_now_add=True)